package com.geovanedsilveira.app_empresas.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.geovanedsilveira.app_empresas.R;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
