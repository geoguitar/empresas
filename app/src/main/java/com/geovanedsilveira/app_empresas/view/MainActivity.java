package com.geovanedsilveira.app_empresas.view;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.geovanedsilveira.app_empresas.R;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = " ";
    public static final String API = "http://empresas.ioasys.com.br";


    private EditText entradaNome, entradaSenha;
    private Button botaoEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InicializarLogin();

        botaoEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nome = entradaNome.getText().toString();
                String senha = entradaSenha.getText().toString();

                Toast.makeText(getApplicationContext(), "Ok "+ nome +" você digitou sua senha "+ senha,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void InicializarLogin() {
        entradaNome = findViewById(R.id.editNomeLogin);
        entradaSenha = findViewById(R.id.editSenhaLogin);
        botaoEntrar = findViewById(R.id.btEntrarLogin);
    }
}
