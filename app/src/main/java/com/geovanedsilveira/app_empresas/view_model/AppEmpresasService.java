package com.geovanedsilveira.app_empresas.view_model;

import com.geovanedsilveira.app_empresas.view_model.AppEmpresasResponse;

import retrofit2.Call;
import retrofit2.http.POST;

public interface AppEmpresasService {
    public String URL = "http://empresas.ioasys.com.br";
    @POST("")
    Call<AppEmpresasResponse> listaEmpresas();
}
