package com.geovanedsilveira.app_empresas.view_model;

import retrofit2.Call;
import retrofit2.http.GET;


public interface AppEmpresasResponse {
    public String URL = "http://empresas.ioasys.com.br";
    @GET("")
    Call<AppEmpresasResponse> listaEmpresas();
}

