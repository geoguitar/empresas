package com.geovanedsilveira.app_empresas.model;

public class UserLogin {

    private  String accesToken ;
    private  String client ;
    private  int uid ;

    public String getAccesToken() {
        return accesToken;
    }

    public String getClient() {
        return client;
    }

    public int getUid() {
        return uid;
    }

}
